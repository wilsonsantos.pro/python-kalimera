# pylint: disable=too-few-public-methods
from dataclasses import dataclass, field
from typing import List

from .events import Event


@dataclass
class Entity:
    events: List[Event] = field(default_factory=lambda: [], init=False)


class ValueObject:
    pass
