from functools import wraps
from typing import Any, Callable, Optional

from .interface import IRepository
from .typing import T


def attr_getter(func: Callable[..., Optional[T]]) -> Callable[..., Optional[T]]:
    """Decorator that adds the found object to the "seen" set, if found."""

    @wraps(func)
    def wrapper(self: IRepository[T], *args: Any, **kwargs: Any) -> Optional[T]:
        obj: Optional[T] = func(self, *args, **kwargs)
        if obj:
            self.seen.add(obj)
        return obj

    return wrapper
