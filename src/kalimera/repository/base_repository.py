"""Generic repository interface."""
import abc
from typing import Any, Generic, Optional, Set

from .decorators import attr_getter
from .pagination import Pagination
from .typing import T


class AbstractRepository(abc.ABC, Generic[T]):
    """Generic repository interface."""

    def __init__(self) -> None:
        self.seen: Set[T] = set()

    def add(self, obj: T) -> None:
        """Add the object to the repository."""
        self._add(obj)
        self.seen.add(obj)

    @attr_getter
    def get(self, **search_fields: Any) -> Optional[T]:
        """Get the object from the repository.

        Args:
            search_fields: search fields as kwargs.

        Returns:
            The object, if found or None.
        """
        return self._get(**search_fields)

    def get_list(
        self,
        page: Optional[int] = None,
        per_page: Optional[int] = None,
        count: bool = True,
    ) -> Pagination[T]:
        return self._get_list(page, per_page, count)

    @abc.abstractmethod
    def _add(self, obj: T) -> None:
        pass

    @abc.abstractmethod
    def _get(self, **search_fields: Any) -> Optional[T]:
        pass

    @abc.abstractmethod
    def _get_list(
        self,
        page: Optional[int] = None,
        per_page: Optional[int] = None,
        count: bool = True,
    ) -> Pagination[T]:
        pass
