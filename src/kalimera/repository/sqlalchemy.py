from typing import TYPE_CHECKING, Any, Optional, Type

from sqlalchemy import func, select

from .base_repository import AbstractRepository
from .pagination import Pagination
from .typing import T

if TYPE_CHECKING:
    from sqlalchemy.orm import Session


class BaseSqlAlchemyRepository(AbstractRepository[T]):
    """Base repository implementation using SqlAlchemy."""

    model_cls: Type[T]

    def __init__(self, session: "Session") -> None:
        super().__init__()
        self.session: "Session" = session

    def _add(self, obj: T) -> None:
        self.session.add(obj)

    def _get(self, **search_fields: Any) -> Optional[T]:
        return self.session.scalars(
            select(self.model_cls).filter_by(**search_fields)
        ).first()

    def _get_list(
        self,
        page: Optional[int] = None,
        per_page: Optional[int] = None,
        count: bool = True,
    ) -> Pagination[T]:
        stmt = select(self.model_cls)
        if not count:
            total = None
        else:
            total = self.session.scalar(
                select(func.count()).select_from(self.model_cls)
            )
        if page and per_page:
            stmt = stmt.limit(per_page).offset((page - 1) * per_page)
        items_per_page = self.session.execute(stmt).scalars().all()
        return Pagination(
            items=items_per_page,
            page_number=page,
            per_page=per_page,
            count=total,
        )
