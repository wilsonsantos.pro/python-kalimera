from dataclasses import dataclass
from typing import Generic, Iterable, Optional

from .typing import T

PageItems = Iterable[T]


@dataclass(frozen=True)
class Pagination(Generic[T]):
    items: PageItems[T]
    page_number: Optional[int]
    per_page: Optional[int]
    count: Optional[int]
