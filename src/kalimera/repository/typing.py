from typing import TypeVar

from kalimera.domain import model

T = TypeVar("T", bound=model.Entity)  # pylint: disable=invalid-name
E = TypeVar("E")  # pylint: disable=invalid-name
