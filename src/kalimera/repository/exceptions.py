class MissingSearchFieldError(Exception):
    """When a search field is missing on the query request."""

    def __init__(self, /, missing_field: str):
        message = f"Missing field '{missing_field}'"
        super().__init__(message)
