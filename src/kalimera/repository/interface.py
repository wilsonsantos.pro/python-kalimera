from typing import Protocol, Set

from .typing import T


class IRepository(Protocol[T]):
    seen: Set[T]
