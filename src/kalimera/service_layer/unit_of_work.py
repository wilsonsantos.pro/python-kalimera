import abc
from typing import Any, Generator, Generic

from kalimera import repository
from kalimera.domain.events import Event
from kalimera.repository.typing import T


class AbstractUnitOfWork(abc.ABC, Generic[T]):
    objects: repository.AbstractRepository[T]

    def __enter__(self) -> "AbstractUnitOfWork[T]":
        return self

    def __exit__(self, *args: Any) -> None:
        self.rollback()

    def collect_new_events(self) -> Generator[Event, None, None]:
        for obj in self.objects.seen:
            while obj.events:
                yield obj.events.pop(0)

    @abc.abstractmethod
    def commit(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self) -> None:
        raise NotImplementedError
