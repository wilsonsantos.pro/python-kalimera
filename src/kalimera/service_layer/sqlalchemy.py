from typing import Any, Callable

from sqlalchemy.orm import Session

from kalimera import repository
from kalimera.repository.typing import T

from .unit_of_work import AbstractUnitOfWork


class SqlAlchemyUnitOfWork(AbstractUnitOfWork[T]):
    def __init__(self, session_factory: Callable[[], Session]):
        self.session_factory = session_factory
        self.session: Session

    def __enter__(self) -> "AbstractUnitOfWork[T]":
        self.session = self.session_factory()
        self.objects = repository.BaseSqlAlchemyRepository(self.session)
        return super().__enter__()

    def __exit__(self, *args: Any) -> None:
        super().__exit__(*args)
        self.session.close()

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
