# pylint: disable=missing-function-docstring,missing-class-docstring,redefined-outer-name,duplicate-code,unused-argument,invalid-name,redefined-builtin
"""Tests for kalimera/repository."""
from dataclasses import dataclass
from typing import Dict, Optional

import pytest

from kalimera.domain.model import Entity
from kalimera.repository import AbstractRepository
from kalimera.repository.pagination import Pagination


@dataclass
class DummyObj(Entity):
    """This is dummy."""

    id: int
    name: str
    age: int

    def __hash__(self) -> int:
        return hash(f"{self.id}{self.name}")


class DummyRepository(AbstractRepository[DummyObj]):
    _objs: Dict[int, DummyObj]

    def __init__(self) -> None:
        super().__init__()
        self._objs = {}

    def _add(self, obj: DummyObj) -> None:
        self._objs[obj.id] = obj

    def _get(self, **search_fields: int) -> Optional[DummyObj]:
        id = search_fields.get("id")
        if id:
            return self._objs[id]
        return None

    def _get_list(
        self,
        page: Optional[int] = None,
        per_page: Optional[int] = None,
        count: bool = True,
    ) -> Pagination[DummyObj]:
        total = len(self._objs) if count else 0
        items_per_page = self._objs
        return Pagination(
            items=list(items_per_page.values()),
            page_number=page,
            per_page=per_page,
            count=total,
        )


@pytest.fixture
def dummy_obj() -> DummyObj:
    return DummyObj(id=1, name="arst", age=27)


@pytest.fixture
def repository() -> AbstractRepository[DummyObj]:
    return DummyRepository()


@pytest.fixture
def obj_from_repository(
    repository: AbstractRepository[DummyObj], dummy_obj: DummyObj
) -> Optional[DummyObj]:
    repository.add(dummy_obj)
    return repository.get(id=dummy_obj.id)


def test_obj_is_persisted(
    obj_from_repository: Optional[DummyObj], dummy_obj: DummyObj
) -> None:
    assert obj_from_repository == dummy_obj


def test_obj_is_seen(
    repository: AbstractRepository[DummyObj],
    obj_from_repository: Optional[DummyObj],
    dummy_obj: DummyObj,
) -> None:
    """Test repository.decorators.attr_getter()"""
    assert dummy_obj in repository.seen
