# pylint: disable=redefined-outer-name,invalid-name
from dataclasses import dataclass
from typing import Generator, Optional

import pytest
import sqlalchemy
from sqlalchemy.orm import Session, clear_mappers, registry, sessionmaker

from kalimera.domain.model import Entity
from kalimera.repository import BaseSqlAlchemyRepository


@pytest.fixture(autouse=True)
def mappers() -> Generator[None, None, None]:
    start_mappers()
    yield
    clear_mappers()


@pytest.fixture
def in_memory_sqlite_db() -> sqlalchemy.engine.Engine:
    engine = sqlalchemy.create_engine("sqlite:///:memory:")
    metadata.create_all(engine)
    return engine


@pytest.fixture
def sqlite_session_factory(in_memory_sqlite_db):  # type: ignore
    yield sessionmaker(bind=in_memory_sqlite_db)


@pytest.fixture
def dbsession(sqlite_session_factory):  # type: ignore
    dbsession: Optional[Session] = None
    try:
        dbsession = sqlite_session_factory()
        yield dbsession
    finally:
        if dbsession:
            dbsession.close()


metadata = sqlalchemy.MetaData()

notes = sqlalchemy.Table(
    "notes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("text", sqlalchemy.String),
    sqlalchemy.Column("completed", sqlalchemy.Boolean),
)


@dataclass
class Note(Entity):
    id: int
    text: str
    completed: bool

    def __hash__(self) -> int:
        return hash(id)


def start_mappers() -> None:
    mapper_reg = registry()
    mapper_reg.map_imperatively(Note, notes)


class DummySqlAlchemyRepository(BaseSqlAlchemyRepository[Note]):
    model_cls = Note


def test_get(dbsession):  # type: ignore
    repo = DummySqlAlchemyRepository(dbsession)
    note1 = Note(id=1, text="text1", completed=True)
    repo.add(note1)
    assert repo.get(id=1) == note1


def test_get_by_text(dbsession):  # type: ignore
    repo = DummySqlAlchemyRepository(dbsession)
    note1 = Note(id=1, text="text1", completed=True)
    repo.add(note1)
    assert repo.get(text="text1") == note1


def test_get_list(dbsession):  # type: ignore
    repo = DummySqlAlchemyRepository(dbsession)
    notes = []
    for i in range(1, 11):
        note = Note(id=i, text=f"text{i}", completed=True)
        repo.add(note)
        notes.append(note)

    expected = notes[:3]
    assert repo.get_list(page=1, per_page=3).items == expected
    expected = notes[-2:]
    result = repo.get_list(page=5, per_page=2)
    assert result.items == expected
    assert result.count == len(notes)
